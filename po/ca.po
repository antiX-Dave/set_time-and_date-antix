# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Geoff Gigg <geoffgigg@hotmail.com>, 2021
# Eduard Selma <selma@tinet.cat>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-08 16:56+0200\n"
"PO-Revision-Date: 2021-10-29 12:04+0000\n"
"Last-Translator: Eduard Selma <selma@tinet.cat>, 2021\n"
"Language-Team: Catalan (https://www.transifex.com/anticapitalista/teams/10162/ca/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ca\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: set_time-and_date.sh:20
msgid "Manage Date and Time Settings"
msgstr "Gestiona els paràmetres d'hora i data"

#: set_time-and_date.sh:22
msgid "Date:"
msgstr "Data:"

#: set_time-and_date.sh:23
msgid "Set Current Time"
msgstr "Defineix l'hora actual"

#: set_time-and_date.sh:24
msgid "Set Current Date"
msgstr "Defineix la data actual"

#: set_time-and_date.sh:25
msgid "Choose Time Zone (using cursor and enter keys)"
msgstr "Trieu la Zona Horària (amb el ratolí i la tecla de Retorn)"

#: set_time-and_date.sh:26
msgid "Use Internet Time server to set automaticaly time/date"
msgstr ""
"Usa el servidor d'hora d'internet per ajustar automàticament la data i hora."

#: set_time-and_date.sh:27
msgid "Move the slider to the correct Hour"
msgstr "Useu el botó lliscant per ajustar l'hora correcta"

#: set_time-and_date.sh:28
msgid "Move the slider to the correct Minute"
msgstr "Useu el botó lliscant per ajustar al minut correcte."

#: set_time-and_date.sh:29
msgid "Select Time Zone"
msgstr "Trieu la Zona Horària"

#: set_time-and_date.sh:30
msgid "Quit"
msgstr "Surt"
