��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    &   �     �  &   �  !   �  #        B     J     X     l  8                          	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2021
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 Velg tidssone (med piltaster og Enter) Dato: Behandle innstillinger for dato og tid Flytt glidebryter til riktig time Flytt glidebryter til riktig minutt Avslutt Velg tidssone Velg gjeldende dato Velg gjeldende tid Hent automatisk dato og tid fra tidstjener på Internett 