��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  f    =   �     �  '   �  %   �  $     	   =     G     c     x  A   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: Feri, 2022
Language-Team: Hungarian (https://www.transifex.com/anticapitalista/teams/10162/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 Időzóna kiválasztása (kurzor és az enter billentyűkkel) Dátum: Dátum és időbeállítások kezelése Húzza a csúszkát a pontos órához Húzza a csúszkát a pontos perchez Kilépés Válassza ki az időzónát Dátum beállítása Idő beállítása Internet szerver használata az idő automatikus beállítására 