��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    "   �     �  N   �  I   0  M   z     �  "   �  8   �  *   1  �   \                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2021
Language-Team: Greek (https://www.transifex.com/anticapitalista/teams/10162/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 Επιλέξτε Ζώνη ώρας Ημερομηνία: Διαχείριση ρυθμίσεων ημερομηνίας και ώρας Μετακινήστε το ρυθμιστικό στη σωστή ώρα Μετακινήστε το ρυθμιστικό στο σωστό λεπτό Εξοδος Επιλέξτε Ζώνη ώρας Ορισμός τρέχουσας ημερομηνίας Ορισμός τρέχουσας ώρας Χρησιμοποιήστε το διακομιστή ώρας Internet για να ορίσετε αυτόματα ώρα/ημερομηνία 