��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  ~    <   �     �  &   �  2     5   9     o     t     �     �  M   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2021
Language-Team: Catalan (https://www.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Trieu la Zona Horària (amb el ratolí i la tecla de Retorn) Data: Gestiona els paràmetres d'hora i data Useu el botó lliscant per ajustar l'hora correcta Useu el botó lliscant per ajustar al minut correcte. Surt Trieu la Zona Horària Defineix la data actual Defineix l'hora actual Usa el servidor d'hora d'internet per ajustar automàticament la data i hora. 